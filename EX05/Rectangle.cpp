#include "Rectangle.h"
#include <string>
#include <iostream>

using std::string;
using std::cout;
using std::endl;


/*
	Constructor
	Input:
		point A, length, width, shape type, shape name
	Output:
		None
*/
myShapes::Rectangle::Rectangle(const Point& a, double length, double width, const std::string& type, const std::string& name) :
	Polygon(type, name), _length(length), _width(width)
{
	if (width <= 0 || length <= 0)
	{
		throw "Length or Width must be over 0.\n";
	}
	this->_points.push_back(a);
	this->_points.push_back(Point(a.getX() + width, a.getY() + length));
}

/*
	Destructor
	Input:
		None
	Output:
		None
*/
myShapes::Rectangle::~Rectangle()
{

}


void myShapes::Rectangle::draw(const Canvas& canvas)
{
	canvas.draw_rectangle(_points[0], _points[1]);
}


void myShapes::Rectangle::clearDraw(const Canvas& canvas)
{
	canvas.clear_rectangle(_points[0], _points[1]);
}


/*
	Function calculates and returns the area of the rectangle
	Input:
		None
	Output:
		area of the rectangle
*/
double myShapes::Rectangle::getArea() const
{
	return this->_length * this->_width;
}


/*
	Function calculates and returns the perimeter of the rectangle
		Input:
			None
		Output:
			perimeter of the rectangle
*/
double myShapes::Rectangle::getPerimeter() const
{
	return this->_length * 2 + this->_width * 2;
}


/*
	Function moves the rectangle to another given point
	Input:
		another point
	Output:
		None
*/
void myShapes::Rectangle::move(const Point& other)
{
	this->_points[0].operator+=(other);
	this->_points[1].operator+=(other);
}