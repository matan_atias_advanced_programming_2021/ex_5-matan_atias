#include "Shape.h"
#include <string>
#include <iostream>

using std::string;
using std::cout;
using std::endl;

/*
	Constructor
	Input:
		name of the shape, type of the shape
	Output:
		None
*/
Shape::Shape(const std::string& name, const std::string& type)
{
	this->_name = name;
	this->_type = type;
}

/*
	Destructor
	Input:
		None
	Output:
		None
*/
Shape::~Shape()
{

}

/*
	Function prints the details of a shape
	Input:
		None
	Output:
		None
*/
void Shape::printDetails() const
{
	cout << this->_type << "  " << this->_name << "       " << getArea() << "  " << getPerimeter() << endl;
}

/*
	Function returns the type of the shape
	Input:
		None
	Output:
		Type of the shape
*/
std::string Shape::getType() const
{
	return this->_type;
}


/*
	Function returns the name of the shape
	Input:
		None
	Output:
		Name of the shape
*/
std::string Shape::getName() const
{
	return this->_name;
}