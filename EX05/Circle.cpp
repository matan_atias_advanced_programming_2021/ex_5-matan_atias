#include "Circle.h"
#include <string>
#include <iostream>

using std::string;
using std::cout;
using std::endl;

/*
	Constructor
	Input:
		center, radius, shape type, shape name
	Output:
		None
*/
Circle::Circle(const Point& center, double radius, const std::string& type, const std::string& name) :
	Shape(name, type), _center(center)
{
	if (radius < 0)
	{
		throw "Radius must be over or equal to 0.\n";
	}
	this->_radius = radius;
}

/*
	Destructor
	Input:
		None
	Output:
		None
*/
Circle::~Circle()
{

}

/*
	Function returns center
	Input:
		None
	Output:
		center
*/
const Point& Circle::getCenter() const
{
	return this->_center;
}

/*
	Function returns radius
	Input:
		None
	Output:
		None
*/
double Circle::getRadius() const
{
	return this->_radius;
}


void Circle::draw(const Canvas& canvas)
{
	canvas.draw_circle(getCenter(), getRadius());
}


void Circle::clearDraw(const Canvas& canvas)
{
	canvas.clear_circle(getCenter(), getRadius());
}


/*
	Function calculates the area of the circle and returns it
	Input:
		None
	Output:
		area of the circle
*/
double Circle::getArea() const
{
	return PI * this->_radius * this->_radius;
}


/*
	Function calculates the perimeter of the circle and returns it
	Input:
		None
	Output:
		perimeter of the circle
*/
double Circle::getPerimeter() const
{
	return PI * this->_radius * 2;
}


/*
	Function moves the circle
	Input:
		point to move the circle
	Output:
		None
*/
void Circle::move(const Point& other)
{
	this->_center.operator+=(other);
}
