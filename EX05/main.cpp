#include "Menu.h"


#define ADD_NEW_SHAPE 0
#define MODIFY_OR_GET_INFORMATION 1
#define DELETE_ALL_SHAPES 2
#define EXIT 3

#define CIRCLE 0
#define ARROW 1
#define TRIANGLE 2
#define RECTANGLE 3

#define MOVE 0
#define GET_DETAILS 1
#define REMOVE_SHAPE 2

void main()
{
	int choice = 0;
	int choice1 = 0;
	Menu menu;
	std::vector<Shape*> shapes;
	//declaring vars
	while (true)
	{
		do
		{
			choice = menu.mainMenu();
			system("CLS");
		} while (choice < 0 || choice > 3);
		switch (choice)
		{
			case ADD_NEW_SHAPE:
				do
				{
					choice = menu.shapeMenu();
					system("CLS");
				} while (choice < 0 || choice > 3);
				switch (choice)
				{
					case CIRCLE:
						menu.createCircle(shapes);
						break;
					case ARROW:
						menu.createArrow(shapes);
						break;
					case TRIANGLE:
						menu.createTriangle(shapes);
						break;
					case RECTANGLE:
						menu.createRectangle(shapes);
						break;
					default:
						_exit(1);
				}
				system("CLS");
				break;
			case MODIFY_OR_GET_INFORMATION:
				if (!shapes.empty())
				{
					do
					{
						choice = menu.chooseShapeMenu(shapes);
						system("CLS");
					} while (choice < 0 || choice > shapes.size());
					do
					{
						choice1 = menu.modifyInformationMenu();
						system("CLS");
					} while (choice1 < 0 || choice1 > 2);
					switch (choice1)
					{
						case MOVE:
							system("CLS");
							menu.move_shape(shapes, choice);
							system("CLS");
							break;
						case GET_DETAILS:
							menu.getDetails(shapes, choice);
							system("PAUSE");
							system("CLS");
							break;
						case REMOVE_SHAPE:
							menu.deleteShape(shapes, choice);
							break;
						default:
							_exit(1);
					}
				}
				break;
			case DELETE_ALL_SHAPES:
				menu.deleteAllShapes(shapes);
				system("CLS");
				break;
			case EXIT:
				system("pause");
				_exit(1);
				break;
			defualt:
				_exit(1);
		}
	}
}