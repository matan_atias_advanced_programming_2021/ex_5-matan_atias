#pragma once
#include "Shape.h"
#include "Circle.h"
#include "Triangle.h"
#include "Arrow.h"
#include "Rectangle.h"
#include "Point.h"
#include "Canvas.h"
#include <vector>



class Menu
{
public:

	Menu();
	~Menu();

	int mainMenu();
	int shapeMenu();
	void createCircle(std::vector<Shape*> &shapes);
	void createArrow(std::vector<Shape*> &shapes);
	void createTriangle(std::vector<Shape*> &shapes);
	void createRectangle(std::vector<Shape*> &shapes);
	int chooseShapeMenu(std::vector<Shape*> shapes);
	int modifyInformationMenu();
	void move_shape(std::vector<Shape*> shapes, int index);
	void deleteAllShapes(std::vector<Shape*> &shapes);
	void getDetails(std::vector<Shape*> shapes, int index);
	void deleteShape(std::vector<Shape*> &shapes, int index);
private: 
	Canvas _canvas;
};

