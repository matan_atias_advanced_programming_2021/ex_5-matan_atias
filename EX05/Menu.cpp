#include "Menu.h"
#include <string>
#include <iostream>

using std::string;
using std::cout;
using std::endl;
using std::cin;

/*
	Constructor
	Input:
		None
	Output:
		None
*/
Menu::Menu() 
{
}


/*
	Destructor
	Input:
		None
	Output:
		None
*/
Menu::~Menu()
{
}


/*
	Function prints the main menu and returns user's choice
	Input:
		None
	Output:
		user's choice
*/
int	Menu::mainMenu()
{
	int choice = 0;
	//declaring vars
	cout << "Enter 0 to add a new shape." << endl;
	cout << "Enter 1 to modify or get informantion from a current shape." << endl;
	cout << "Enter 2 to delete all of the shapes." << endl;
	cout << "Enter 3 to exit." << endl;
	cin >> choice;
	return choice;
}


/*
	Function prints the shape menu and returns user's choice
	Input:
		None
	Output:
		user's choice
*/
int Menu::shapeMenu()
{
	int choice = 0;
	//declaring vars
	cout << "Enter 0 to add a circle." << endl;
	cout << "Enter 1 to add an arrow." << endl;
	cout << "Enter 2 to add a triangle." << endl;
	cout << "Enter 3 to add a rectangle." << endl;
	cin >> choice;
	return choice;
}


/*
	Function creates a circle
	Input:
		vector of shapes
	Output:
		None
*/
void Menu::createCircle(std::vector<Shape*> &shapes)
{
	
	int x = 0;
	int y = 0;
	int radius = 0;
	string name = " ";
	//declaring vars
	try
	{
		cout << "Please enter X:" << endl;
		cin >> x;
		cout << "Please enter Y:" << endl;
		cin >> y;
		cout << "Please enter radius:" << endl;
		cin >> radius;
		cout << "Please enter the name of the shape:" << endl;
		cin >> name;
		Circle* circle = new Circle(Point(x, y), radius, "Circle", name);
		shapes.push_back(circle);
		circle->draw(this->_canvas);
	}
	//getting all required data about the shape
	catch (const char* error)
	{
		std::cerr << error << endl;
		system("Pause");
		system("CLS");
	}
}


/*
	Function creates an arrow
	Input:
		vector of shapes
	Output:
		None
*/
void Menu::createArrow(std::vector<Shape*> &shapes)
{
	int x1 = 0;
	int y1 = 0;
	int x2 = 0;
	int y2 = 0;
	string name = " ";
	//declaring vars
	try
	{
		cout << "Enter X of point number: 1" << endl;
		cin >> x1;
		cout << "Enter Y of point number: 1" << endl;
		cin >> y1;
		cout << "Enter X of point number: 2" << endl;
		cin >> x2;
		cout << "Enter Y of point number: 2" << endl;
		cin >> y2;
		cout << "Enter the name of the shape:" << endl;
		cin >> name;
		Arrow* arrow = new Arrow(Point(x1, y1), Point(x2, y2), "Arrow", name);
		shapes.push_back(arrow);
		arrow->draw(this->_canvas);
	}
	//getting all required data about the shape
	catch (const char* error)
	{
		std::cerr << error << endl;
		system("Pause");
		system("CLS");
	}
}


/*
	Function creates a triangle
	Input:
		vector of shapes
	Output:
		None
*/
void Menu::createTriangle(std::vector<Shape*> &shapes)
{
	int x1 = 0;
	int y1 = 0;
	int x2 = 0;
	int y2 = 0;
	int x3 = 0;
	int y3 = 0;
	string name = " ";
	//declaring vars
	try 
	{
		cout << "Enter X of point number: 1" << endl;
		cin >> x1;
		cout << "Enter Y of point number: 1" << endl;
		cin >> y1;
		cout << "Enter X of point number: 2" << endl;
		cin >> x2;
		cout << "Enter Y of point number: 2" << endl;
		cin >> y2;
		cout << "Enter X of point number: 3" << endl;
		cin >> x3;
		cout << "Enter Y of point number: 3" << endl;
		cin >> y3;
		cout << "Enter the name of the shape:" << endl;
		cin >> name;
		Triangle* triangle = new Triangle(Point(x1, y1), Point(x2, y2), Point(x3, y3), "Triangle", name);
		shapes.push_back(triangle);
		triangle->draw(this->_canvas);
	}
	//getting all required data about the shape
	catch (const char* error)
	{
		std::cerr << error << endl;
		system("Pause");
		system("CLS");
	}
}


/*
	Function creates a rectangle
	Input:
		vector of shapes
	Output:
		None
*/
void Menu::createRectangle(std::vector<Shape*> &shapes)
{
	int x = 0;
	int y = 0;
	int length = 0;
	int width = 0;
	string name = " ";
	//declaring vars
	try 
	{
		cout << "Enter the X of the top left corner:" << endl;
		cin >> x;
		cout << "Enter the Y of the top left corner:" << endl;
		cin >> y;
		cout << "Please enter the length of the shape:" << endl;
		cin >> length;
		cout << "Please enter the width of the shape:" << endl;
		cin >> width;
		cout << "Enter the name of the shape:" << endl;
		cin >> name;
		myShapes::Rectangle* rectangle = new myShapes::Rectangle(Point(x,y), length, width, "Rectangle", name);
		shapes.push_back(rectangle);
		rectangle->draw(this->_canvas);
	}
	//getting all required data about the shape
	catch (const char* error)
	{
		std::cerr << error << endl;
		system("Pause");
		system("CLS");
	}
}


/*
	Function prints the created shapes menu and returns user's choice
	Input:
		shapes(vector)
	Output:
		user's choice
*/
int Menu::chooseShapeMenu(std::vector<Shape*> shapes)
{
	unsigned int i = 0;
	int choice = 0;
	//declaring vars
	for (i = 0; i < shapes.size(); i++)
	{
		cout << "Enter " << i << " for " << shapes[i]->getName() << "(" << shapes[i]->getType() << ")" << endl;
	}
	cin >> choice;
	return choice;
}


/*
	Function prints the modify and information menu and returns user's choice
	Input:
		None
	Output:
		User's choice
*/
int Menu::modifyInformationMenu()
{
	int choice = 0;
	//declaring vars
	cout << "Enter 0 to move the shape." << endl;
	cout << "Enter 1 to get its details." << endl;
	cout << "Enter 2 to remove the shape." << endl;
	cin >> choice;
	//getting choice from user
	return choice;
}


/*
	Function moves a shape
	Input:
		shapes(vector), index of shape to move
	Output:
		None
*/
void Menu::move_shape(std::vector<Shape*> shapes, int index)
{
	unsigned int i = 0;
	int x = 0;
	int y = 0;
	//declaring vars
	cout << "Please enter the X moving scale: ";
	cin >> x;
	cout << "Please enter the Y moving scale: ";
	cin >> y;
	//getting new X and Y 
	shapes[index]->clearDraw(this->_canvas);
	shapes[index]->move(Point(x, y));
	//deleting old shape
	for (i = 0; i < shapes.size(); i++)
	{
		shapes[i]->draw(this->_canvas);
	}
}


/*
	Function deletes all shapes
	Input:
		shapes(vector)
	Output:
		None
*/
void Menu::deleteAllShapes(std::vector<Shape*> &shapes)
{
	unsigned int i = 0;
	//declaring vars
	for (i = 0; i < shapes.size(); i++)
	{
		shapes[i]->clearDraw(this->_canvas);
		delete shapes[i];
	}
	shapes.clear();
}


/*
	Function prints details of a shape
	Input:
		shapes(vector), index
	Output:
		None
*/
void Menu::getDetails(std::vector<Shape*> shapes, int index)
{
	cout << shapes[index]->getType() << " " << shapes[index]->getName() << " " << shapes[index]->getArea() << " " << shapes[index]->getPerimeter() << " " << endl;
}


/*
	Function deletes a shape
	Input:
		shapes(vector), index
	Output:
		None
*/
void Menu::deleteShape(std::vector<Shape*> &shapes, int index)
{
	unsigned int i = 0;
	//declaring vars
	shapes[index]->clearDraw(this->_canvas);
	shapes.erase(shapes.begin() + index);
	//deleting old shape
	for (i = 0; i < shapes.size(); i++)
	{
		shapes[i]->draw(this->_canvas);
	}
}