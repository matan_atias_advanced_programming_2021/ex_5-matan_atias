#include "Point.h"
#include <string>
#include <iostream>

using std::string;
using std::cout;
using std::endl;

/*
	Constructor
	Input:
		x,y
	Output:
		None
*/
Point::Point(double x, double y)
{
	if (x < 0 || y < 0)
	{
		throw "x and y must be over 0.\n";
	}
	this->_x = x;
	this->_y = y;
}

/*
	Destructor
	Input:
		None
	Output:
		None
*/
Point::~Point()
{
	
}

/*
	Copy function
	Input:
		other point
	Output:
		None
*/
Point::Point(const Point& other)
{
	this->_x = other.getX();
	this->_y = other.getY();
}

/*
	Function returns a new point(this x + other x, this y + other y)
	Input:
		other point
	Output:
		new point
*/
Point Point::operator+(const Point& other) const
{
	return Point(this->_x + other.getX(), this->_y + other.getY());
}

/*
	Function changes the current point and returns it
	Input:
		other point
	Output:
		new current point
*/
Point& Point::operator+=(const Point& other)
{
	this->_x += other.getX();
	this->_y += other.getY();
	return *this;
}


/*
	Function return x
	Input:
		None
	Output:
		x
*/
double Point::getX() const
{
	return this->_x;
}


/*
	Function returns y
	Input:
		None
	Output:
		y
*/
double Point::getY() const
{
	return this->_y;
}


/*
	Function calculates and returns the distance between 2 points
	Input:
		another point
	Output:
		distance between 2 points
*/
double Point::distance(const Point& other) const
{
	return std::sqrt(pow(this->_x - other.getX(), 2) + pow(this->_y - other.getY(), 2) * 1.0);
}