#include "Arrow.h"
#include <string>
#include <iostream>

using std::string;
using std::cout;
using std::endl;

/*
	Constructor
	Input:	
		point A, point B, shape type, shape name
	Output:
		None
*/
Arrow::Arrow(const Point& a, const Point& b, const std::string& type, const std::string& name):
	Shape(name, type)
{
	this->_points.push_back(a);
	this->_points.push_back(b);
}

/*
	Destructor
	Input:
		None
	Output:
		None
*/
Arrow::~Arrow()
{

}


void Arrow::draw(const Canvas& canvas)
{
	canvas.draw_arrow(_points[0], _points[1]);
}


void Arrow::clearDraw(const Canvas& canvas)
{
	canvas.clear_arrow(_points[0], _points[1]);
}


/*
	Function returns the perimeter of the arrow
	Input:
		None
	Output:
		perimeter of the arrow
*/
double Arrow::getPerimeter() const
{
	return this->_points[0].distance(this->_points[1]);
}


/*
	Function returns the area of the arrow
	Input:
		None
	Output:
		area of the arrow
*/
double Arrow::getArea() const
{
	return 0;
}


/*
	Function moves the arrow
	Input:
		where to move the arrow
	Output:
		None
*/
void Arrow::move(const Point& other)
{
	this->_points[0].operator+=(other);
	this->_points[1].operator+=(other);
}