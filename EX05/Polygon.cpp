#include "Polygon.h"
#include <string>
#include <iostream>

using std::string;
using std::cout;
using std::endl;

/*
	Constructor
	Input:
		type of shape, name of shape
	Output:
		None
*/
Polygon::Polygon(const std::string& type, const std::string& name) :
	Shape(name, type)
{

}
