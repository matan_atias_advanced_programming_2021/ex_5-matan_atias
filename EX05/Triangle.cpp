#include "Triangle.h"
#include <string>
#include <iostream>

using std::string;
using std::cout;
using std::endl;

/*
	Constructor
	Input:
		point A, point B, point C, shape type, shape name
	Output:
		None
*/
Triangle::Triangle(const Point& a, const Point& b, const Point& c, const std::string& type, const std::string& name):
	Polygon(type, name)
{
	if ((a.getX() == b.getX() && a.getX() == c.getX()) || (a.getY() == b.getY() && a.getY() == c.getY()) || (a.getX() == b.getX() && a.getY() == b.getY()) || (a.getX() == c.getX() && a.getY() == c.getY()) || (c.getX() == b.getX() && c.getY() == b.getY()))
	{
		throw "The points entered create a line.\n";
	}
	this->_points.push_back(a);
	this->_points.push_back(b);
	this->_points.push_back(c);
}

/*
	Destructor
	Input:
		None
	Output:
		None
*/
Triangle::~Triangle()
{

}


void Triangle::draw(const Canvas& canvas)
{
	canvas.draw_triangle(_points[0], _points[1], _points[2]);
}


void Triangle::clearDraw(const Canvas& canvas)
{
	canvas.clear_triangle(_points[0], _points[1], _points[2]);
}


/*
	Function calculates and returns the area of the triangle
	Input:
		None
	Output:
		area of the triangle
*/
double Triangle::getArea() const
{
	double a = this->_points[0].distance(this->_points[1]);
	double b = this->_points[1].distance(this->_points[2]);
	double c = this->_points[2].distance(this->_points[0]);
	double s = (a + b + c) / 2;
	//declaring vars
	return sqrt(s * (s - a) * (s - b) * (s - c));
}


/*
	Function calculates and returns the perimeter of the triangle
	Input:
		None
	Output:
		perimeter of the triangle
*/
double Triangle::getPerimeter() const
{
	double a = this->_points[0].distance(this->_points[1]);
	double b = this->_points[1].distance(this->_points[2]);
	double c = this->_points[2].distance(this->_points[0]);
	//declaring vars
	return a + b + c;
}


/*
	Function moves the triangle to another given point
	Input:
		another point
	Output:
		None
*/
void Triangle::move(const Point& other)
{
	Canvas canvas;
	this->_points[0].operator+=(other);
	this->_points[1].operator+=(other);
	this->_points[2].operator+=(other);
	this->clearDraw(canvas);
	this->draw(canvas);
}